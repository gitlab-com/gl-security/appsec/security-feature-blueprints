---
status: proposed
creation-date: "2023-11-22"
authors: [ "@ameyadarshan", "@fvpotvin", "@rshambhuni" ]
approvers: [ "@vdesousa", "@ankelly" ]
owning-stage: "~devops::govern"
---

**NOTE**: A list of merge requests targeted at implementing DPoP in GitLab can be found [here](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/?sort=created_date&state=all&label_name%5B%5D=DPoP&first_page_size=20).

# Sender Constraining Personal Access Tokens 

## Summary

This document describes a mechanism to sender-constrain Personal Access Tokens (PATs) via a proof-of-possession mechanism at the application level, using SSH keys (public/private key pairs) a user would already be using with GitLab. 
This mechanism eliminates the unintentional PAT disclosure problem (to be called "PAT leak" for short, through this document) as it binds the PAT to a user's SSH public key and thereby requiring attackers to prove possession of the corresponding SSH private key when using the PAT. 
This constrains the PAT to the user who generated it, as only they have access to their SSH private key and gives the GitLab backend, that is receiving the PAT, added assurance that it is indeed the user who generated the PAT that is using it and no one else.

We take inspiration from [RFC 9449: Demonstrating Proof of Possession (DPoP)](https://www.rfc-editor.org/rfc/rfc9449.html) in solving this problem. 
While RFC 9449 describes the DPoP mechanism to sender-constrain OAuth 2.0 access tokens, the DPoP mechanism in principle, will apply to PATs as well. 
This is because we use PATs the same way OAuth clients use their access tokens, which is as bearer tokens. 
Anyone in possession of the PAT can use it. So, the DPoP mechanism can be applied to PATs as well.
The DPoP mechanism, in theory, can be used with other token types as well, depending on their implementation. We chose to apply it for PATs in this blueprint.

## Motivation

The primary aim of sender-constraining Personal Access Tokens (PATs) is to prevent unauthorized or illegitimate parties from using leaked or stolen PATs, by binding a PAT to a public key upon issuance and requiring that the sender proves possession of the corresponding private key when using the PAT.

At GitLab we have implemented various security hardening measures to make access tokens more secure. To start with, we made [UI changes](https://gitlab.com/gitlab-org/gitlab/-/issues/348660 "Populate default expiration and pre-select least privilege scopes when creating new access tokens") that sets a default 1 month expiration and pre-selects least-privilege scopes/roles when creating access tokens. 
We have enforced [lifetime limits on access tokens](https://about.gitlab.com/blog/2023/10/25/access-token-lifetime-limits/). 
We have implemented mechanisms like [Automatic Token Reuse Detection](http://docs.gitlab.com/ee/api/personal_access_tokens.html#automatic-reuse-detection) that prevents attackers from maintaining indefinite access to a user’s account, by automatically revoking the leaked PATs, if the API detects a reuse of those tokens. 
We have created tools like [Tokinator](https://gitlab.com/gitlab-com/gl-security/appsec/tokinator) that proactively detects and alerts SIRT about leaked access tokens.

Leaked PATs may have an expiration for up to a year and an attacker in possession of such a leaked PAT can still maintain access to that user's account and the organization that user is associated with, for that period of time. 
APIs don't normally authenticate who is sending the PAT via requests as they just check to see if the PAT hasn't expired and is authorized to perform the action mentioned in the request and let it through. 
So, the backend has no way of knowing if it is the legitimate user with their PAT or if it is the attacker using a leaked PAT that is making the request. 
If a leaked PAT belongs to a high-privileged (Maintainer or above) user in that organization, it could have significant consequences. 
We have identified this security hardening issue in [this security feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/425130) and proposed that we leverage existing mechanisms like DPoP to eliminate this issue completely. 
This blueprint has implementation details of that security feature. 
The Application Security and Product Security Engineering teams are working to implement this security feature (see [this overarching epic](https://gitlab.com/groups/gitlab-com/gl-security/appsec/-/epics/38) that tracks this implementation work).

## Proposal

The proposal here is that the user sends a DPoP proof JSON Web Token (JWT) that is sent as a header in an HTTP request, to the REST API, as described in detail below. 
The user uses a DPoP proof JWT to prove the possession of their SSH private key corresponding to their SSH public key. 
This proposal requires that a user has generated a public/private key pair of [supported SSH key type](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types) and have [added the public key to their GitLab account](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account).

This proposal would work well for users who would be working from systems where their SSH keys would be accessible locally. 
It may not work well for scenarios where the API calls originate from a Runner, for example, where SSH keys may not be present.

A DPoP proof is a signature over:
- a timestamp
- a unique identifier
- a hash of the associated PAT that is sent with the API request, and
- an optional API-provided nonce

```mermaid
sequenceDiagram
 User->>+DPoP proof generator CLI: 1. Generate DPoP proof (JWT)
 DPoP proof generator CLI->>-User: 2. Return generated DPoP proof
 User->>+REST API: 3. Request to the REST API with PAT and <br/>DPoP proof in their respective headers
 Note over REST API : Validate DPoP proof
 REST API-->>-User: 4. Return response if the DPoP proof is valid
```

The basic steps of a request to a REST API endpoint with DPoP (without the optional nonce) are shown in the sequence diagram above:
1. User uses a DPoP proof generator CLI to generate the DPoP proof (JWT). 
The DPoP proof generation capability will be integrated into the [`glab` CLI](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/).
Users interested in using the DPoP feature need to install the `glab` CLI and follow the guidance in glab's documentation to generate DPoP proofs.
The CLI takes 2 arguments as input: the path to the SSH private key and the PAT that is going to be sent with the API request. 
2. Once these 2 arguments are passed, the CLI would generate the DPoP proof JWT (that includes a timestamp, unique identifier, hash of the PAT and the SSH public key which is extracted from the corresponding SSH private key) and signs it with the SSH private key and returns the DPoP proof JWT as the output to the CLI command.
3. The user now includes this DPoP proof JWT as the value of a separate header called `DPoP` and sends the request to a REST API endpoint they would like to access, sending along the PAT in it's own header, as they would ordinarily do. 
The API backend would verify if the SSH public key on the user's account matches with the SSH public key mentioned in the DPoP proof. It also verifies that the PAT hash in the DPoP proof matches the PAT presented in the request.
4. The API backend refuses to serve the request if the signature check fails or if the data in the DPoP proof is wrong, e.g., the PAT hash in the DPoP proof does not match the PAT presented in the request. 
The PAT itself, of course, must also be valid in all other respects.

## Design and Implementation Details

We are suggesting that the DPoP feature be added as a setting under the [User profile](https://docs.gitlab.com/ee/user/profile/) section that interested users can enable to protect all API access through their PAT.

As shown in the sequence diagram above, there are 2 aspects to the implementation that we need to work on.
1. User creates a DPoP proof by calling a proof generator CLI which generates the proof for them. We need to work on developing such a DPoP proof generator CLI.
2. The user then uses the generated DPoP proof in a header in their request to the REST API. The API backend then validates the DPoP proof and responds with the requested data if the validation succeeds. We need to add such validation code in the API backend.

Before we go more into the implementation details, let's look at how the DPoP proof JWT looks like, what claims to include in it and what those claims mean.

### The DPoP HTTP Header

A DPoP proof is included in an HTTP request using the following request header field.

`DPoP`: A JWT that adheres to the structure and syntax of [section below](#dpop-proof-jwt-syntax).

The following text shows an example HTTP request, to the GitLab's REST API, that has the `DPoP` HTTP header. The example uses `\` line wrapping per [RFC8792](https://www.rfc-editor.org/rfc/rfc8792.html).

```
POST /api/v4/projects HTTP/1.1
Host: gitlab.com
PRIVATE-TOKEN: <personal-access-token>
DPoP: eyJhbGciOiJSUzUxMiIsImp3ayI6eyJlIjoiQVFBQiIsIm4iOiIwYVY4OXl3N19rZmZ0WFNla3JjOVotby1BZFJKODVmYWxEV0JmcFhNVldzWEdDRTJSM2NXSGRjeGFPZD\
cyR2NDWDBnYXgwZk5YSmtaQzJKRXAtNV9rNDQ3TnRXNnplREVwU3k5ZEswRmJuemhVZ0hPcmhMdXRYTDEtSkliVnFmb3hjTzdvMEZCcE9EWkdRdHNVTGxYUXZrX0R4VGpjNWhtM1\
NsSkYydjBpUnkxMXd4R25tbEpPUVowek5qMFBBRkxHdWpoTUpvYU95SHZPTDFtd0o4NGhaZVdBTFlVMjVmVUdzdTBraG5VVGhLSGFwdjA0akNhZ3Y1cFItT0tQVUtMbFhiMU9ibH\
lzVF9oakFiT1o3YkVPN0g1VGM5VzFTOGNNQlAtb0NiUVV5MWhQNHBZZDJaY3BsbzJENnpxUXFfVXpNVTFNQno4d2RuNHNSRlFkVWUweFEiLCJrdHkiOiJSU0EifSwia2lkIjoiU0\
hBMjU2OitHdXNGckhCZWE0WjRlYWtubDc0VUM1UDBlcCtmYmIvNnMzOHRDQ2VoeDgiLCJ0eXAiOiJkcG9wK2p3dCJ9.eyJleHAiOjE3MTQzNzAwODksImlhdCI6MTcxNDM2OTc4OSwianRpIjoiZjY2NWZjYWYtNDQyOC00MDBhLTgyNDAtODQ0Mjc0YzcxYzE2IiwiaHRtIjoiIiwiaHR1IjoiIiwiYXRo\
IjoiMmdIYzVULXlDWTFVUlpGU0xtRWlfV3RzSHVDZDhKdU1zNlpGanE0RTV0TSJ9.rxGS_XNxJ7ia6lYI5Dgepsm1Xopzcg9z_dD7F2tYOgGBrpgP3UACTBcd3VODrD1e-4sc4XEFMTD_rqjUx93t9zbkRdf3MBzlxJRpToR8aQ1NSpDkTrLyEDVhLrcr0_2k9HDF-fLw\
z1JTm7Q1ihKXSUzxzZAGRiRbD0u5335Uz9lbkhPsxyTAZRoyRF7swSS5WsUzSTy_t5Y9iAiGKkh3yABqkFFmRvgoR8sjP8fJILCnTLhOdN7RyYdPQPA65ycIGPTjeMmdRMS9SUf-\
bj3x72Td6KWhqu29JPJsv1ymbPcmqBp-ZpW0OoacgEg0YeuIRiZj5fNobVgTu1Lmlk3FKQ
Content-Type: application/json

{"name": "new_project", "description": "New Project", "path": "new_project", "namespace_id": "42", "initialize_with_readme": "true"}
```

### DPoP Proof JWT Syntax

A DPoP proof is a JWT that is signed (using JSON Web Signature (JWS) mechanism) with a user's SSH private key.

The [JOSE Header](https://datatracker.ietf.org/doc/html/rfc7515#section-2) of a DPoP JWT **MUST** contain at least the following parameters:

`typ`: Stands for [`Type`](https://datatracker.ietf.org/doc/html/rfc7515#section-4.1.9). 
A field with the value `dpop+jwt`, as recommended in [Section 4.2 of RFC9449](https://www.rfc-editor.org/rfc/rfc9449.html#section-4.2).

`alg`: Stands for [`Algorithm`](https://datatracker.ietf.org/doc/html/rfc7515#section-4.1.1). 
An identifier for a JWS asymmetric digital signature algorithm from [JSON Web Signature and Encryption Algorithms](https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms). 
It **MUST NOT** be `none`. 
It **MUST NOT** be an identifier for a symmetric algorithm (Message Authentication Code (MAC)).

`jwk`: Stands for [`JSON Web Key`](https://datatracker.ietf.org/doc/html/rfc7515#section-4.1.3). 
Represents the public key chosen by the user in JSON Web Key (JWK) format as defined in [JSON Web Key Parameters](https://www.iana.org/assignments/jose/jose.xhtml#web-key-parameters). 
It **MUST NOT** contain a private key.

`kid`: Stands for [`Key ID`](https://datatracker.ietf.org/doc/html/rfc7515#section-4.1.4). 
A field whose value must be the result of base64url encoding (as defined in [Section 2 of RFC7515](https://www.rfc-editor.org/rfc/rfc7515#section-2)) the SHA-256 hash of user's public key.

The payload of a DPoP proof **MUST** contain at least the following claims:

`jti`: Stands for [`JWT ID`](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.7). 
Unique identifier for the DPoP proof JWT. 
The value **MUST** be assigned such that there is a negligible probability that the same value will be assigned to any other DPoP proof used in the same context during the time window of validity. 
Such uniqueness can be accomplished by encoding (base64url or any other suitable encoding) at least 96 bits of pseudorandom data or by using a version 4 Universally Unique Identifier (UUID) string. 
The `jti` can be used by the server for replay detection and prevention, see [Section 11.1 of RFC9449](https://www.rfc-editor.org/rfc/rfc9449.html#section-11.1).

`iat`: Stands for [`Issued At`](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.6). 
Creation timestamp of the JWT, see [Section 4.1.6 of RFC7519](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.6).

`exp`: Stands for [`Expiration Time`](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4). 
Expiration timestamp of the JWT, see [Section 4.1.4 of RFC7519](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4).
We can mitigate the DPoP proof replay attacks by using the `exp` (expiration time) claim itself and checking at the backend to see if the [current time](https://www.epochconverter.com/) is past the `exp`. 
To define how the `exp` time needs to be set, we are suggesting adding `5 minutes` to the `iat` and setting that as the value for `exp`.
This would mean the DPoP proofs would only be valid for `5 minutes` since they were generated (`iat`) and users will have to generate new DPoP proofs every `5 minutes`. 
The DPoP specification (see [Section 11.1 of RFC9449](https://www.rfc-editor.org/rfc/rfc9449.html#section-11.1)) recommends the same to prevent DPoP proof replay.

> To limit this, servers MUST only accept DPoP proofs for a limited time after their creation (preferably only for a relatively brief period on the order of seconds or minutes).

`ath`: Stands for [`Access Token Hash`](https://www.rfc-editor.org/rfc/rfc9449.html#section-4.2). 
The value **MUST** be the result of base64url encoding (as defined in [Section 2 of RFC7515](https://www.rfc-editor.org/rfc/rfc7515#section-2)) the SHA-256 hash of the ASCII encoding of the associated PAT's value.

The payload of a DPoP proof **MAY** contain the following claims:

`htm`: Stands for `HTTP Method`. 
The value of the HTTP method (see [Section 9.1 of RFC9110](https://www.rfc-editor.org/rfc/rfc9110#section-9.1)) of the request to which the JWT is attached.

`htu`: Stands for `HTTP URI`. 
The HTTP target URI (see [Section 7.1 of RFC9110](https://www.rfc-editor.org/rfc/rfc9110#section-7.1)) of the request to which the JWT is attached, without query and fragment parts.

If these claims are set, a user's access to the REST API would be restricted to just the HTTP method described in `htm` and the URI/API endpoint defined in `htu` claims and nothing else. 

For example, assume `htm` is `POST` and `htu` is `https://gitlab.com/api/v4/projects`, the user can **only** make `POST` calls to `https://gitlab.com/api/v4/projects` with their PAT and the DPoP proof that has these claims. 
If they would like to make API calls to any other endpoint or make other state-changing request to the same endpoint, they must generate a new DPoP proof everytime they want to make such requests.

While this is great from a fine-grained access control perspective, it may be bad from a UX perspective. 
We are proposing that we skip implementing `htm` and `htu` claims for the first few iterations in the rollout of the DPoP feature as having these claims will make the API access **very** restrictive. 
In the future, if there happen to be customers/users who are interested in such fine-grained access control to the API, we can consider supporting these claims.

`nonce`: A recent nonce provided via the `DPoP-Nonce` HTTP header. See [Section 9 of RFC9449](https://www.rfc-editor.org/rfc/rfc9449.html#name-resource-server-provided-no) for more information regarding the implementation of this parameter. 

The REST API backend will generate the `nonce` in our context and this claim is a defense-in-depth measure against replaying DPoP proofs. 
For the first iteration, we can skip implementing this `nonce` claim as it requires generating and storing nonce per user per request which is an additional overhead. 

The following JSON content shows the base64 decoded DPoP proof JWT. The JSON of the JWT header and payload are shown, but the signature part is omitted. 
The example uses `\` line wrapping per [RFC8792](https://www.rfc-editor.org/rfc/rfc8792.html).

```json
{
  "alg": "RS256",
  "typ": "dpop+jwt",
  "jwk": {
    "kty": "RSA",
    "e": "AQAB",
    "n": "vhctjC9MQNOGL0H2tToITl71qXy2mWgd_ND6gB1frlNf4zWMN9aSQvCeU6PdBus2L7B-7mdc4-_vcOgsbCOWE1Xbq3hOhoDhLJQuvieAg3AAMo3uq--69CZ\
pHhNbDC4KloyAXmL7uEKbl1l-FXYMZNEW7eWaxHO6B6wvD5DU7owniOCqqUEOiNO5DVhj_Jt0oOigD0PwnvIn2OHm_vAwHGEd64X5emz5s8vzniuMl4I96uD2-_4VIWfL\
YeSmGCzKCpNXhyzfqGXVhTpyGQ8D-NH6uo2aNUSqvR8PDNit44I8vVG8_bvWmJsV2xQx4z-kpy5IhKk8WC722kqpNgIy4Q",
    "kid": "SHA256:+GusFrHBea4Z4eaknl74UC5P0ep+fbb/6s38tCCehx8"
  }
}
.
{
  "jti": "e1j3V_bKic8-LAEB",
  "iat": 1562262618,
  "exp": 1562262918
  "ath": "fUHyO2r2Z3DZ53EsNrWBb0xWXoaNy59IiKCAqksmQEo"
}
```

The following sections describe what changes we need to make on the client-side (user) and the server-side (API backend) to rollout the DPoP feature.

### Client Side Implementation

We have to provide a CLI that users can use to generate a DPoP proof JWT before they make a request to the API. 
The invocation of the CLI and the arguments passed to it would be as follows:

```sh
dpop_proof_generator_cli -pvt $PATH_TO_SSH_PRIVATE_KEY -pat $PERSONAL_ACCESS_TOKEN
```

The CLI would take 2 arguments as shown above:
1. `PATH_TO_SSH_PRIVATE_KEY` - this is needed to sign the DPoP proof JWT and to extract the SSH public key which would be represented in the `jwk` claim in the DPoP proof JWT. 
This would let the backend know which SSH public key (on the user's profile) must be used to validate the JWT's signature.
1. `PERSONAL_ACCESS_TOKEN` - this is needed as we need to hash the `PERSONAL_ACCESS_TOKEN` and include that hash in the `ath` claim of the DPoP proof JWT.

The output of the CLI command above would return the DPoP proof JWT which the user must include as the value of the `DPoP` header in the request to the REST API, as discussed in the [section above](#the-dpop-http-header).

### Server Side Implementation

Before the API backend performs any actions with the PAT that is sent with the request, it must validate both PAT validity and DPoP proof to make sure that the sender is able to confirm the proof-of-possession of that PAT.

To validate a DPoP proof, the API backend **MUST** ensure the following:

1. There is not more than one `DPoP` HTTP request header field.
1. The `DPoP` HTTP request header field value is a single and well-formed JWT.
1. All required claims as described in the [DPoP Proof JWT Syntax](#dpop-proof-jwt-syntax) section above are contained in the JWT.
1. The `typ` JOSE Header Parameter has the value `dpop+jwt`.
1. The `alg` JOSE Header Parameter:
   1. is a registered asymmetric digital signature algorithm (see [JSON Web Signature and Encryption Algorithms](https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms))
   1. is not `none`
   1. is [supported](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types) by the application
1. The `jwk` JOSE Header Parameter does not contain a private key.
1. The fingerprint of the public key contained in the `jwk` JOSE Header Parameter matches with the fingerprint of [any one of the SSH public keys](https://docs.gitlab.com/ee/api/users.html#list-ssh-keys-for-user) on the user profile that are only of the type `signing` or `auth_and_signing`.
   1. Only if the above check succeeds, verify the JWT signature with the public key contained in the `jwk` JOSE Header Parameter.
1. The expiration time of the JWT, as determined by the `exp` claim, is not past the current time, as discussed under `exp` claim description in the [DPoP Proof JWT Syntax](#dpop-proof-jwt-syntax) section above.
1. Ensure that the value of the `ath` claim equals the base64url encoding of the SHA-256 hash of the ASCII encoding of the PAT sent with the API request, as described in the [DPoP Proof JWT Syntax](#dpop-proof-jwt-syntax) section above.

The API backend **MUST NOT** grant access to the requested resource unless all checks are successful.

#### Request-Response Examples Using DPoP

The following text shows a response to an API request with invalid PAT:

```
GET /protectedresource HTTP/1.1
Host: gitlab.com
Private-Token: INVALID_TOKEN
DPoP: VALID_DPoP_PROOF

HTTP/1.1 401 Unauthorized
WWW-Authenticate: Bearer error="invalid_token", error_description="Invalid token", DPoP algs="EdDSA RS256"
```

The following text shows a response to an API request with valid PAT and invalid DPoP proof (as per the [proof validation criteria](#server-side-implementation) described above):

```
GET /protectedresource HTTP/1.1
Host: gitlab.com
Private-Token: VALID_TOKEN
DPoP: INVALID_DPoP_PROOF

HTTP/1.1 401 Unauthorized
WWW-Authenticate: DPoP error="invalid_dpop_proof", error_description="Invalid DPoP proof", DPoP algs="EdDSA RS256"
```

The following text shows a response to an API request with valid PAT, valid DPoP proof but the DPoP proof is bound to a PAT that is different to the PAT sent in the request:

```
GET /protectedresource HTTP/1.1
Host: gitlab.com
Private-Token: VALID_TOKEN
DPoP: VALID_DPoP_PROOF

HTTP/1.1 401 Unauthorized
WWW-Authenticate: DPoP error="invalid_token", error_description="Invalid DPoP key binding", DPoP algs="EdDSA RS256"
```

**Note:** We were not able to map all [supported SSH key types](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types) to the corresponding `alg` in the [JWS terminology](https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms).

- `ED25519` maps to `EdDSA` algorithm in JWS. See [Section 3.1 of RFC8037](https://www.rfc-editor.org/rfc/rfc8037.html#section-3.1) for reference
- `ED25519_SK` may map to `EdDSA` as well? Not sure. WE NEED TO VERIFY THIS!
- `ECDSA_SK` - Not sure what algorithm in JWS it maps to. WE NEED TO CONFIRM THIS!
- `RSA` maps to either `RS256`, `RS384` and `RS512` algorithms in JWS. See [Section 3.3 in RFC7518](https://www.rfc-editor.org/rfc/rfc7518.html#section-3.3) for reference

## Alternative Solutions

Another approach to sender-constrain PATs is through mTLS and certificate-bound PATs. 
[RFC8705: OAuth 2.0 Mutual-TLS Client Authentication and Certificate-Bound Access Tokens](https://www.rfc-editor.org/rfc/rfc8705.html) discusses the same for OAuth 2.0 access tokens but the same in principle would apply to PATs as well, similar to how DPoP applies to our case, as described in the [Summary](#summary) section above.

To briefly describe how sender-constraining with certificate-bound PATs work - Users must authenticate themselves to the GitLab API using mutual TLS, based on either self-signed certificates or certificates generated through a Certificate Authority (CA) from Public Key Infrastructure (PKI) setup. 
Mutual-TLS certificate-bound access tokens ensure that only the party in possession of the private key corresponding to the certificate can utilize the token to access the associated resources.

The major drawback with this approach is the overhead in setting up and maintaining certificates both on the user side and on the GitLab's side.
- If the user wants to use a CA to generate the cert, we need to provide them with the PKI setup to accomplish that. 
- Or, we can rely on self-signed certificates from users but the user has to register that generated certificate with GitLab for the application to know what the user's public key (for their certificate) is. So, we also need to make changes to the user's profile to add a section where they can upload their generated certificates. We also have to make the API backend changes to support certificate-bound PATs.

For these reasons, if we can leverage existing public/private key pairs a user would have already generated (SSH key pairs) and shared with GitLab (the SSH public key) we can use them to accomplish what we are doing (pinning a PAT to a user using their SSH public key) which makes Demonstrating Proof of Possession (DPoP) the easier choice among the two as the problem of public/private key pair generation and distribution is already addressed and we can just focus on the DPoP feature.

Other security hardening features for PATs, like [fine-grained access control for PATs](https://gitlab.com/gitlab-org/gitlab/-/issues/368904) can be addressed through the `htm` and `htu` claims, as discussed in the [DPoP Proof JWT Syntax](#dpop-proof-jwt-syntax) section above. A security hardening feature like [fine-grained scopes for PATs](https://gitlab.com/gitlab-org/gitlab/-/issues/351223) cannot be implemented through this feature and has to be implemented separately.
